$("[data-toogle='popover']").popover();
$('.carousel').carousel({
  interval: 3000
});
$('.reservar').on('show.bs.modal', function (e) {
  console.log('El modal se esta mostrando');
  $('#contactoBtn').remove('border-success');
  $('#contactoBtn').addClass('btn-primary');
  $('#contactoBtn').prop('disabled', true);
});
$('.reservar').on('shown.bs.modal', function (e) {
  console.log('El modal reserva se mostro');
});
$('.reservar').on('hide.bs.modal', function (e) {
  console.log('El modal se esta ocultando');
});
$('.reservar').on('hidden.bs.modal', function (e) {
  console.log('El modal se oculto');
  $('#contactoBtn').prop('disabled', false);
});